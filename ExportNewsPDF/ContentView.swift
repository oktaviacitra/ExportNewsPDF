//
//  ContentView.swift
//  ExportNewsPDF
//
//  Created by Oktavia Citra on 08/12/21.
//

import SwiftUI

struct ContentView: View {
    private let size: CGFloat = UIScreen.main.bounds.width - 30
    
    var body: some View {
        NavigationView {
            VStack(alignment: .leading) {
                title
                subtitle
                picture
                paragraph
                button
                Spacer()
            }
            .padding()
        }
    }
    
    var title: some View {
        Text("Lorem Ipsum Dolor Sit Amet")
            .font(.title)
            .fontWeight(.semibold)
    }
    
    var subtitle: some View {
        Text("consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
            .font(.subheadline)
    }
    
    var picture: some View {
        Image("example")
            .resizable()
            .scaledToFill()
            .frame(width: size, height: (size / 4 * 3))
            .cornerRadius(15.0)
    }
    
    var paragraph: some View {
        Text("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet dictum sit amet justo donec enim diam vulputate. Pellentesque habitant morbi tristique senectus et netus. Magna etiam tempor orci eu lobortis elementum nibh tellus molestie. Tristique senectus et netus et malesuada fames. Ut ornare lectus sit amet est placerat in egestas. Varius duis at consectetur lorem donec massa sapien faucibus. Pellentesque habitant morbi tristique senectus et netus et malesuada.")
            .font(.caption)
    }
    
    var button: some View {
        NavigationLink(destination: DisplayPDFView(url: makePDF())) {
            HStack {
                Spacer()
                
                Text("Export to PDF")
                    .font(.subheadline)
                
                Spacer()
            }
            .padding()
        }
    }
    
    func makePDF() -> URL {
        let outputFileURL: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent("News.pdf")
        
        let pageSize: CGSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let bounds: CGRect = CGRect(origin: .zero, size: pageSize)
        
        let contentVC: UIHostingController = UIHostingController(rootView: ContentView())
        contentVC.view.frame = bounds
        
        let rootVC = UIApplication.shared.windows.first?.rootViewController
        rootVC?.addChild(contentVC)
        rootVC?.view.insertSubview(contentVC.view, at: 0)
        
        let format: UIGraphicsPDFRendererFormat = UIGraphicsPDFRendererFormat()
        format.documentInfo = [
            kCGPDFContextCreator: "Oktavia Citra",
            kCGPDFContextAuthor: "gitlab/oktaviacitra"
          ] as [String: Any]
        let pdfRenderer: UIGraphicsPDFRenderer = UIGraphicsPDFRenderer(bounds: bounds, format: format)
        
        DispatchQueue.main.async {
            do {
                try pdfRenderer.writePDF(to: outputFileURL, withActions: { context in
                    context.beginPage()
                    rootVC?.view.layer.render(in: context.cgContext)
                })
            } catch {
                print("Could not create PDF file: \(error)")
            }
        }
        
        contentVC.removeFromParent()
        contentVC.view.removeFromSuperview()
        
        return outputFileURL
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 11")
    }
}
