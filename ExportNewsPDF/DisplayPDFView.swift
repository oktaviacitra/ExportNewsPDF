//
//  DisplayPDFView.swift
//  ExportNewsPDF
//
//  Created by Oktavia Citra on 09/12/21.
//

import SwiftUI

struct DisplayPDFView: View {
    var url: URL
    
    var body: some View {
        PDFKitRepresentedView(url)
            .navigationBarItems(trailing: Button(action: shareFile){ Text("Share") })
    }
    
    private func shareFile() {
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        UIApplication.shared.windows.first?.rootViewController?.present(activityViewController, animated: true, completion: nil)
    }
}

//struct DisplayPDFView_Previews: PreviewProvider {
//    static var previews: some View {
//        DisplayPDFView()
//    }
//}
