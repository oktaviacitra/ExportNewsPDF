//
//  ExportNewsPDFApp.swift
//  ExportNewsPDF
//
//  Created by Oktavia Citra on 08/12/21.
//

import SwiftUI

@main
struct ExportNewsPDFApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
